extends Area2D

@onready var tiempo_de_congelacion = $TiempoDeCongelacion
@onready var sprite_2d = $Sprite2D
@onready var particulas_nieve = $ParticulasDeNieve

var enemigo_actual
var activado = false
var permitir_activar = true

@export var color_congelacion: Color

func _on_tiempo_de_congelacion_timeout():
	if enemigo_actual != null:
		enemigo_actual.collision_shape_2d.set_deferred("disabled", false)
		enemigo_actual.rapidez = 150
		enemigo_actual.actualizar_color(enemigo_actual.color)
		enemigo_actual.animation_player.play()
		enemigo_actual = null
		permitir_activar = true

func _on_area_entered(area):
	if area.is_in_group("Enemigo") && activado:
		area.collision_shape_2d.set_deferred("disabled", true)
		activado = false
		permitir_activar = false
		enemigo_actual = area
		tiempo_de_congelacion.start()
		area.rapidez = 0
		sprite_2d.frame = 1
		particulas_nieve.emitting = true
		area.actualizar_color(color_congelacion)
		area.animation_player.pause()

func activar_maquina():
	if permitir_activar && !activado:
		activado = true
		sprite_2d.frame = 0

func _on_body_entered(body):
	if body.is_in_group("Jugador"):
		activar_maquina()
