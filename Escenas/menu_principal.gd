extends Node2D

@onready var animation_player_zumbito = $Zumbito/AnimationPlayer
@onready var animation_player_humo = $Humo/AnimationPlayer
@onready var sprite_2d_humo = $Humo/Sprite2D
@onready var opciones = $Opciones

@export var color: Color

func _ready():
	animation_player_zumbito.play("mosquito_menu")
	animation_player_humo.play("humo_menu")
	sprite_2d_humo.modulate = color


func _on_boton_play_button_up():
	get_tree().change_scene_to_file("res://Escenas/escena_principal.tscn")


func _on_boton_exit_button_up():
	get_tree().quit()


func _on_boton_opciones_button_up():
	opciones.visible = true
