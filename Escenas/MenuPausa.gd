extends Control
@onready var opciones = $"../Opciones"
@onready var game_manager = $"../.."


func _on_boton_play_button_up():
	game_manager.pausar()


func _on_boton_opciones_button_up():
	opciones.visible = true
	

func _on_boton_exit_button_up():
	get_tree().quit()
