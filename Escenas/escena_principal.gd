extends Node2D

@onready var personaje_mosquito = $"Personaje-mosquito"
@onready var game_over_canvas = $"Game Over Canvas"
@onready var boton_reinicio = $"Game Over Canvas/BotonReiniciar"
@onready var boton_salir = $"Game Over Canvas/BotonSalir"
@onready var boton_pausa = $UI/BotonPausa
@onready var mano1 = $Manos/Mano
@onready var mano2 = $Manos/Mano2
@onready var mano3 = $Manos/Mano3
@onready var mano4 = $Manos/Mano4
@onready var timer = $"Timers/Timer Respawn"
@onready var barra_de_sangre = $"UI/Barra de sangre"
@onready var barra_de_sangre_porcentaje = $"UI/Barra de sangre porcentaje"
@onready var texto_game_over = $"Game Over Canvas/Texto Game Over"
@onready var sonido_mosquito = $Audio/SonidoMosquito
@onready var musica = $Audio/Musica
@onready var menu_pausa = $UI/MenuPausa
@onready var opciones = $UI/Opciones

@export var nivel_de_alerta = 0
@export var sangre_total = 0
@export var enemigos: Array[Area2D]
@export var maquinas_congeladoras: Array[Area2D]
@export var spawn_enemigos: Marker2D

var array_manos = []
var juego_finalizado = false
var tecla_izq
var tecla_der
var tecla_abajo
var tecla_arriba
var tecla_picar

func _ready():
	boton_reinicio.pressed.connect(func(): reiniciar())
	boton_salir.pressed.connect(func(): get_tree().quit())
	boton_pausa.pressed.connect(func(): pausar())
	array_manos = [mano1, mano2, mano3, mano4]

func _input(event):
	if Input.is_action_just_pressed("pausar"):
		pausar()

func perdiste():
	texto_game_over.text = "Perdiste"
	personaje_mosquito.visible = false
	game_over_canvas.visible = true
	Engine.time_scale = 0
	musica.stop()
	boton_pausa.disabled = true
	boton_pausa.visible = false
	menu_pausa.visible = false
	juego_finalizado = true

func ganaste():
	texto_game_over.text = "Ganaste!"
	personaje_mosquito.visible = false
	game_over_canvas.visible = true
	Engine.time_scale = 0
	musica.stop()
	boton_pausa.disabled = true
	boton_pausa.visible = false
	menu_pausa.visible = false
	juego_finalizado = true

func pausar():
	opciones.visible = false
	if !juego_finalizado:
		if menu_pausa.visible:
			Engine.time_scale = 1
			habilitar_teclas()
		else:
			Engine.time_scale = 0
			deshabilitar_teclas()
		menu_pausa.visible = !menu_pausa.visible

func deshabilitar_teclas():
	tecla_izq = InputMap.action_get_events("izquierda")
	tecla_der = InputMap.action_get_events("derecha")
	tecla_abajo = InputMap.action_get_events("abajo")
	tecla_arriba = InputMap.action_get_events("arriba")
	tecla_picar = InputMap.action_get_events("picar")
	InputMap.action_erase_events("izquierda")
	InputMap.action_erase_events("derecha")
	InputMap.action_erase_events("abajo")
	InputMap.action_erase_events("arriba")
	InputMap.action_erase_events("picar")

func habilitar_teclas():
	for cada_tecla in tecla_izq:
		InputMap.action_add_event("izquierda", cada_tecla)
	for cada_tecla in tecla_der:
		InputMap.action_add_event("derecha", cada_tecla)
	for cada_tecla in tecla_abajo:
		InputMap.action_add_event("abajo", cada_tecla)
	for cada_tecla in tecla_arriba:
		InputMap.action_add_event("arriba", cada_tecla)
	for cada_tecla in tecla_picar:
		InputMap.action_add_event("picar", cada_tecla)

func reiniciar():
	Engine.time_scale = 1
	get_tree().change_scene_to_file("res://Escenas/escena_principal.tscn")

func perder_vida():
	sonido_mosquito.play()
	for enemigo in enemigos:
		enemigo.rapidez = 0
	for maquina in maquinas_congeladoras:
		maquina.tiempo_de_congelacion.stop()
		if !maquina.activado:
			maquina.permitir_activar = true

func respawnear():
	personaje_mosquito.visible = true
	personaje_mosquito.rapidez = 600
	personaje_mosquito.position = Vector2(0, 200)
	personaje_mosquito.direccionMovimiento = Vector2.ZERO
	personaje_mosquito.siguienteDireccionMovimiento = Vector2.ZERO
	personaje_mosquito.rotation_degrees = 0
	for enemigo in enemigos:
		enemigo.rapidez = 150
		enemigo.global_position = spawn_enemigos.global_position
		enemigo.index_actual = 0
		enemigo.actualizar_color(enemigo.color)
		enemigo.animation_player.play()
		enemigo.inicio_moverse_en_patron()
		enemigo.collision_shape_2d.set_deferred("disabled", false)

	for mano in array_manos:
		if mano.sangre_disponible > 0:
			mano.sprite_2d.texture = load("res://Sprites/mano-abierta.png")

	nivel_de_alerta = 0

func _on_timer_timeout():
	respawnear()
	timer.stop()

func _process(delta):
	nivel_de_alerta -= 20 * delta
	
	barra_de_sangre.value = sangre_total
	barra_de_sangre_porcentaje.value = sangre_total
	
	if sangre_total >= 100:
		ganaste()
