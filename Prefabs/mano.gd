extends Area2D

@onready var game_manager = $"../.."
@onready var sprite_2d = $Sprite2D
@onready var collision_shape_2d = $CollisionShape2D
@onready var jugador = $"../../Personaje-mosquito"
@onready var barra_espaciadora = $BarraEspaciadora
@onready var animacion_barra = $BarraEspaciadora/AnimacionBarra
@onready var barra_de_alerta = $"Barra de alerta"
@onready var sonido_succion = $SonidoSuccion

@export var picando = false
var colisionando = false
@export var sangre_disponible = 25
@export var barra_arriba = false

@export var color_tranqui: Color
@export var color_precaucion: Color
@export var color_peligro: Color

func _ready():
	if barra_arriba:
		barra_espaciadora.position = Vector2(0, -62)
	else:
		barra_espaciadora.position = Vector2(0, 62)

func _on_body_entered(body):
	if body.is_in_group("Jugador"):
		colisionando = true
		posicionarse_para_picar()

func _on_body_exited(body):
	if body.is_in_group("Jugador"):
		colisionando = false
		picando = false

func _input(event):
	if(picando) && (Input.is_action_just_pressed("izquierda") || Input.is_action_just_pressed("derecha") || Input.is_action_just_pressed("abajo") || Input.is_action_just_pressed("arriba")):
		picando = false
		jugador.rapidez = 600
		
	if picando && Input.is_action_just_pressed("picar"):
		picar()
		
	if colisionando && picando == false && Input.is_action_just_pressed("picar"):
		posicionarse_para_picar()
	
func posicionarse_para_picar():
	if sangre_disponible > 0:
		jugador.position = position
		jugador.rapidez = 0
		picando = true

func picar():
	sonido_succion.play()
	game_manager.nivel_de_alerta += 7
	sangre_disponible -= 0.5
	game_manager.sangre_total += 0.5

	if sangre_disponible <= 0:
		picando = false
		jugador.rapidez = 600
		sprite_2d.texture = load("res://Sprites/mano-muerta.png")
		collision_shape_2d.visible = false


func _process(delta):
	if !jugador.visible:
		colisionando = false
		picando = false
	
	if colisionando:
		if game_manager.nivel_de_alerta < 30:
			barra_de_alerta.tint_progress = color_tranqui
		elif game_manager.nivel_de_alerta >= 30 && game_manager.nivel_de_alerta < 70: 
			barra_de_alerta.tint_progress = color_precaucion
		elif game_manager.nivel_de_alerta >= 70:
			barra_de_alerta.tint_progress = color_peligro
	
	if picando:
		game_manager.nivel_de_alerta = clamp(game_manager.nivel_de_alerta, 0, 100)
		barra_espaciadora.visible = true
		animacion_barra.play("barra")
		if game_manager.nivel_de_alerta >= 90:
			colisionando = false
			picando = false
			sprite_2d.texture = load("res://Sprites/mano-cerrada.png")
			jugador.perder_vida()

	else:
		animacion_barra.pause()
		barra_espaciadora.visible = false
	
	barra_de_alerta.visible = colisionando
	barra_de_alerta.value = game_manager.nivel_de_alerta
