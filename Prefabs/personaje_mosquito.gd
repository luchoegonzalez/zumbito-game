extends CharacterBody2D

class_name PersonajeMosquito

@export var rapidez = 600
@export var vidas = 3

var direccionMovimiento = Vector2.ZERO
var siguienteDireccionMovimiento = Vector2.ZERO
var raycast = PhysicsShapeQueryParameters2D.new()
var particulas_de_muerte = preload("res://Prefabs/particulas_de_muerte.tscn")

@onready var collision_shape_2d = $CollisionShape2D
@onready var animation_player = $AnimationPlayer
@onready var game_manager = $".."
@onready var timer = $"../Timers/Timer Respawn"
@onready var texto_vidas = $"../UI/Texto vidas"

func _ready():
	raycast.shape = collision_shape_2d.shape
	raycast.collide_with_areas = false
	raycast.collide_with_bodies = true
	raycast.collision_mask = 2
	animation_player.play("default")

func _physics_process(delta):
	obtener_input()
	
	if direccionMovimiento == Vector2.ZERO:
		direccionMovimiento = siguienteDireccionMovimiento
	if puedoMoverme(siguienteDireccionMovimiento, delta):
		direccionMovimiento = siguienteDireccionMovimiento
	
	velocity = rapidez * direccionMovimiento
	move_and_slide()

func obtener_input():
	if Input.is_action_just_pressed("izquierda"):
		siguienteDireccionMovimiento = Vector2.LEFT
		rotation_degrees = -90
	elif Input.is_action_just_pressed("derecha"):
		siguienteDireccionMovimiento = Vector2.RIGHT
		rotation_degrees = 90
	elif Input.is_action_just_pressed("abajo"):
		siguienteDireccionMovimiento = Vector2.DOWN
		rotation_degrees = 180
	elif Input.is_action_just_pressed("arriba"):
		siguienteDireccionMovimiento = Vector2.UP
		rotation_degrees = 0

func puedoMoverme(direccion: Vector2, delta: float) -> bool:
	raycast.transform = global_transform.translated(direccion * rapidez * delta * 2)
	var resultado = get_world_2d().direct_space_state.intersect_shape(raycast)
	return resultado.size() == 0
	

func perder_vida():
	vidas -= 1
	if vidas <= 0:
		texto_vidas.text = "x"
		game_manager.perdiste()
	else:
		var instancia_particulas = particulas_de_muerte.instantiate()
		game_manager.add_child(instancia_particulas)
		instancia_particulas.global_position = global_position
		instancia_particulas.emitting = true
		game_manager.perder_vida()
		global_position = Vector2(99999999999,99999999999)
		visible = false
		rapidez = 0
		timer.start()
		texto_vidas.text = str(vidas - 1)
