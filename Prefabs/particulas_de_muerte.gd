extends CPUParticles2D

@onready var autodestruccion = $Autodestruccion

func _ready():
	autodestruccion.start()

func _on_autodestruccion_timeout():
	queue_free()
