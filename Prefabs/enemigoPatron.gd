extends Area2D

class_name EnemigoPatron

var index_actual = 0

@export var rapidez = 150
@export var targets: Array[Node2D]
@export var tile_map: TileMap
@export var color: Color
@export var sprite: Sprite2D

@onready var navigation_agent_2d = $NavigationAgent2D
@onready var animation_player = $AnimationPlayer
@onready var collision_shape_2d = $CollisionShape2D

func _ready():
	sprite.modulate = color
	animation_player.play("enemigo_patron")
	
	navigation_agent_2d.path_desired_distance = 8.0
	navigation_agent_2d.target_desired_distance = 8.0
	navigation_agent_2d.target_reached.connect(posicion_alcanzada)
	
	set_physics_process(false)
	await get_tree().physics_frame
	set_physics_process(true)
	
	call_deferred("setear")

func _physics_process(delta):
	mover_enemigo(navigation_agent_2d.get_next_path_position(), delta)

func mover_enemigo(siguiente_pos: Vector2, delta: float):
	var pos_actual = global_position
	
	var nueva_velocidad = (siguiente_pos - pos_actual).normalized() * rapidez * delta
	position += nueva_velocidad

func setear():
	navigation_agent_2d.set_navigation_map(tile_map.get_navigation_map(0))
	NavigationServer2D.agent_set_map(navigation_agent_2d.get_rid(), tile_map.get_navigation_map(0))
	inicio_moverse_en_patron()

func inicio_moverse_en_patron():
	navigation_agent_2d.target_position = targets[index_actual].global_position

func posicion_alcanzada():
	if index_actual < targets.size() - 1:
		index_actual += 1
	else:
		index_actual = 0
	inicio_moverse_en_patron()

func _on_body_entered(body):
	if body.is_in_group("Jugador"):
		body.perder_vida()

func actualizar_color(nuevo_color: Color):
	sprite.modulate = nuevo_color
