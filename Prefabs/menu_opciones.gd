extends Control

@onready var MUSICA_BUS_ID = AudioServer.get_bus_index("Musica")
@onready var SFX_BUS_ID = AudioServer.get_bus_index("SFX")
@onready var volumen_musica = $VolumenMusica
@onready var volumen_sonido = $VolumenSonido

func _ready():
	volumen_musica.value = db_to_linear(AudioServer.get_bus_volume_db(MUSICA_BUS_ID))
	volumen_sonido.value = db_to_linear(AudioServer.get_bus_volume_db(SFX_BUS_ID))

func _on_volumen_musica_value_changed(value):
	AudioServer.set_bus_volume_db(MUSICA_BUS_ID, linear_to_db(value))
	AudioServer.set_bus_mute(MUSICA_BUS_ID, value < 0.05)


func _on_volumen_sonido_value_changed(value):
	AudioServer.set_bus_volume_db(SFX_BUS_ID, linear_to_db(value))
	AudioServer.set_bus_mute(SFX_BUS_ID, value < 0.05)


func _on_boton_retroceder_pressed():
	visible = false
