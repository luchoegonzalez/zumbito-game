extends Area2D

@onready var animation_player_espiral = $AnimationPlayerEspiral

func _ready():
	animation_player_espiral.play("espiral_loop")
	

func _on_body_entered(body):
	if body.is_in_group("Jugador"):
		body.perder_vida()
