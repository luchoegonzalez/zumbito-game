extends Area2D

class_name EnemigoSeguidor

enum EstadoEnemigo {
	PATRON,
	PERSEGUIR
}

var index_actual = 0
var estado_actual: EstadoEnemigo

@export var rapidez = 150
@export var distancia_de_persecucion = 150
@export var targets: Array[Node2D]
@export var tile_map: TileMap
@export var color: Color
@export var objetivo_de_persecucion: Node2D
@export var tiempo_movimiento_patron: float
@export var tiempo_persecucion: float

@onready var navigation_agent_2d = $NavigationAgent2D
@onready var animation_player = $AnimationPlayer
@onready var sprite = $SpriteEnemigo
@onready var timer_persecucion = $"Timer Persecucion"
@onready var timer_finalizar_patron = $TimerFinalizarPatron
@onready var timer_finalizar_persecucion = $TimerFinalizarPersecucion
@onready var collision_shape_2d = $CollisionShape2D

func _ready():
	sprite.modulate = color
	timer_finalizar_patron.wait_time = tiempo_movimiento_patron
	timer_finalizar_persecucion.wait_time = tiempo_persecucion
	
	animation_player.play("enemigo_patron")
	
	navigation_agent_2d.path_desired_distance = 8.0
	navigation_agent_2d.target_desired_distance = 8.0
	navigation_agent_2d.target_reached.connect(posicion_alcanzada)
	
	set_physics_process(false)
	await get_tree().physics_frame
	set_physics_process(true)
	
	call_deferred("setear")

func _physics_process(delta):
	mover_enemigo(navigation_agent_2d.get_next_path_position(), delta)

func mover_enemigo(siguiente_pos: Vector2, delta: float):
	var pos_actual = global_position
	
	var nueva_velocidad = (siguiente_pos - pos_actual).normalized() * rapidez * delta
	position += nueva_velocidad

func setear():
	navigation_agent_2d.set_navigation_map(tile_map.get_navigation_map(0))
	NavigationServer2D.agent_set_map(navigation_agent_2d.get_rid(), tile_map.get_navigation_map(0))
	inicio_moverse_en_patron()

func inicio_moverse_en_patron():
	timer_persecucion.stop()
	timer_finalizar_patron.stop()
	timer_finalizar_persecucion.stop()
	timer_finalizar_patron.start()
	estado_actual = EstadoEnemigo.PATRON
	navigation_agent_2d.target_position = targets[index_actual].global_position

func moverse_en_patron():
	navigation_agent_2d.target_position = targets[index_actual].global_position

func posicion_alcanzada():
	if estado_actual == EstadoEnemigo.PATRON:
		if index_actual < targets.size() - 1:
			index_actual += 1
		else:
			index_actual = 0
		moverse_en_patron()
	elif estado_actual == EstadoEnemigo.PERSEGUIR:
		print("mosquito muerto")

func _on_body_entered(body):
	if body.is_in_group("Jugador"):
		body.perder_vida()

func _on_timer_finalizar_patron_timeout():
	comenzar_a_perseguir()

func comenzar_a_perseguir():
	if objetivo_de_persecucion == null:
		print("no hay objetivo para perseguir")
		
	estado_actual = EstadoEnemigo.PERSEGUIR
	timer_persecucion.start()
	timer_finalizar_persecucion.start()
	navigation_agent_2d.target_position = objetivo_de_persecucion.global_position


func _on_timer_persecucion_timeout():
	var distancia_a_sig_punto = global_position.distance_to(objetivo_de_persecucion.global_position)
	if distancia_a_sig_punto < distancia_de_persecucion:
		inicio_moverse_en_patron()
	else:
		navigation_agent_2d.target_position = objetivo_de_persecucion.global_position


func _on_timer_finalizar_persecucion_timeout():
	timer_persecucion.stop()
	inicio_moverse_en_patron()
	
func actualizar_color(nuevo_color: Color):
	sprite.modulate = nuevo_color
