# Zumbito Game

<img src="https://img.itch.zone/aW1nLzE1MDU1OTgxLnBuZw==/315x250%23c/QTenz5.png" style="display: block; margin: 0 auto">

:es: Toma el control de un mosquito en este emocionante juego 2D al estilo de Pac-Man. Tu misión es picar manos sin ser aplastado por ellas, mientras evitas las peligrosas bolas de humo que rondan por el mapa.

Hecho en Godot 4 como una prueba para la admisión al laboratorio de innovación CAETI LIVE.

:us: Get into the shoes of a mosquito in this exciting 2D Pac-man style game. Your mission is to bite hands without being squashed by them, while avoiding the dangerous smoke balls that prowl around the map.

Made in Godot 4 as a test for admission to the CAETI LIVE innovation lab.

[Play on itch.io](https://lucirro.itch.io/zumbito)